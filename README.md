# Hibernate - Spring cache adapter
A library for using cache adapter of Spring Framework as a second level distributed cache in Hibernate.

# Maven dependency

To install it, you just need to add the following Maven dependency. Masked version lib {hibernate-version}.x, example for hibernate-core 5.2.10.Final is 5.2.10.1
Check Versions for the right version for you:
```xml
<dependency>
    <groupId>ru.gns34</groupId>
    <artifactId>hibernate-spring-cache</artifactId>
    <version>5.2.10.1</version>
</dependency>
```

# Configuration options 

## Targeting the spring cache
 
| Property                                  | Default Value |
|-------------------------------------------|---------------|
| hibernate.springсache.servers               | hibernate 	|
 
 
## Targeting the cache implementation (global or region specific)
 
| Property                                  | Default Value |
|-------------------------------------------|---------------|
| hibernate.springсache.cacheTimeSeconds    |300        	|
| hibernate.springсache.clearSupported      |false      	|
| hibernate.springсache.keyStrategy         | Sha1KeyStrategy |


In order to specify a property for a specific region add the region name right after memcached. ex: hibernate.spring.cache.myregion.cacheTimeSeconds

# Known issues
- Hibernate caache
- Spring framework
- Spring framework cache

# Acknowledgements
* Based on the work done on hibernate-l2-memcached by Mihai Costin
    * https://raw.githubusercontent.com/mihaicostin/hibernate-l2-memcached
