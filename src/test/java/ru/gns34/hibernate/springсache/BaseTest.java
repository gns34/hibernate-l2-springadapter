package ru.gns34.hibernate.springсache;

import org.springframework.context.support.GenericXmlApplicationContext;

public abstract class BaseTest {

    static {
        @SuppressWarnings("resource")
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.getEnvironment().setActiveProfiles("test");
        context.load("classpath:testSpringContext.xml");
        context.refresh();
    }
}
