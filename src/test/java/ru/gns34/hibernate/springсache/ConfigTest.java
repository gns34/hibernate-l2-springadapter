package ru.gns34.hibernate.springсache;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import org.junit.Test;

public class ConfigTest extends BaseTest {

	@Test
    public void testDefaultProperties() {
        final Properties p = new Properties();

        final CacheConfig config = new CacheConfig(p);
        assertEquals("hibernate-entity", config.getCacheName(CacheNamespace.ENTITY, null));
        assertEquals("hibernate-timestamps", config.getCacheName(CacheNamespace.TIMESTAMPS, null));
        assertEquals("hibernate-query", config.getCacheName(CacheNamespace.QUERYRESULTS, null));
        assertEquals("hibernate-naturalid", config.getCacheName(CacheNamespace.NATURALID, null));
        assertEquals("hibernate-collection", config.getCacheName(CacheNamespace.COLLECTION, null));
        
        assertEquals("hibernate-entity", config.getCacheName(CacheNamespace.ENTITY, "ru.gns34.entity.Contract"));
        assertEquals("hibernate-timestamps", config.getCacheName(CacheNamespace.TIMESTAMPS, "org.hibernate.cache.spi.UpdateTimestampsCache"));
        assertEquals("hibernate-query", config.getCacheName(CacheNamespace.QUERYRESULTS, "contact.findByFirstNameAndLastName"));
        assertEquals("hibernate-naturalid", config.getCacheName(CacheNamespace.NATURALID, "Contract#1"));
        assertEquals("hibernate-collection", config.getCacheName(CacheNamespace.COLLECTION, "Contract"));
        
        assertEquals("hibernate-entity", config.getCacheName(CacheNamespace.ENTITY, "ru.gns34.entity.Person"));
        assertEquals("hibernate-timestamps", config.getCacheName(CacheNamespace.TIMESTAMPS, "org.hibernate.cache.spi.UpdateTimestampsCache"));
        assertEquals("hibernate-query", config.getCacheName(CacheNamespace.QUERYRESULTS, "contact.findByFirstNameAndLastNameAndBirthday"));
        assertEquals("hibernate-naturalid", config.getCacheName(CacheNamespace.NATURALID, "PERSON#12"));
        assertEquals("hibernate-collection", config.getCacheName(CacheNamespace.COLLECTION, "PERSON"));
        
        assertEquals(300, config.getLockTimeouts(null));
        assertEquals(300, config.getLockTimeouts("ru.gns34.entity.Contract"));
        assertEquals(300, config.getLockTimeouts("ru.gns34.entity.Person"));
        assertEquals(300, config.getLockTimeouts("contact.findByFirstNameAndLastName"));
    }
	
	@Test
    public void testCacheName() {
        final Properties p = new Properties();
        p.put("hibernate.springсache.name.for.entity", "none-entity");
        p.put("hibernate.springсache.name.for.timestamps", "none-timestamps");
        p.put("hibernate.springсache.name.for.query", "none-query");
        p.put("hibernate.springсache.name.for.naturalid", "none-naturalid");
        p.put("hibernate.springсache.name.for.collection", "none-collection");
        
        p.put("hibernate.springсache.name.for.REGION1.entity", "region1");
        p.put("hibernate.springсache.name.for.REGION2.entity", "region2");
        p.put("hibernate.springсache.name.for.REGION3.query", "region3");
        p.put("hibernate.springсache.name.for.REGION4.query", "region4");
        

        final CacheConfig config = new CacheConfig(p);
        assertEquals("hibernate-none-entity", config.getCacheName(CacheNamespace.ENTITY, null));
        assertEquals("hibernate-none-timestamps", config.getCacheName(CacheNamespace.TIMESTAMPS, null));
        assertEquals("hibernate-none-query", config.getCacheName(CacheNamespace.QUERYRESULTS, null));
        assertEquals("hibernate-none-naturalid", config.getCacheName(CacheNamespace.NATURALID, null));
        assertEquals("hibernate-none-collection", config.getCacheName(CacheNamespace.COLLECTION, null));
        
        assertEquals("hibernate-region1", config.getCacheName(CacheNamespace.ENTITY, "REGION1"));
        assertEquals("hibernate-region2", config.getCacheName(CacheNamespace.ENTITY, "REGION2"));
        assertEquals("hibernate-none-entity", config.getCacheName(CacheNamespace.ENTITY, "REGION---"));
        
        assertEquals("hibernate-region3", config.getCacheName(CacheNamespace.QUERYRESULTS, "REGION3"));
        assertEquals("hibernate-region4", config.getCacheName(CacheNamespace.QUERYRESULTS, "REGION4"));
        assertEquals("hibernate-none-query", config.getCacheName(CacheNamespace.QUERYRESULTS, "REGION---"));
    }
	
	
    @Test
    public void testLockTimeout() {
        final Properties p = new Properties();
        p.put("hibernate.springсache.lock.timeout", "10");
        p.put("hibernate.springсache.REGION1.lock.timeout", "20");
        p.put("hibernate.springсache.REGION2.lock.timeout", "30");

        final CacheConfig config = new CacheConfig(p);
        assertEquals(10, config.getLockTimeouts(null));
        assertEquals(20, config.getLockTimeouts("REGION1"));
        assertEquals(30, config.getLockTimeouts("REGION2"));
        assertEquals(10, config.getLockTimeouts("REGION---"));
    }

}
