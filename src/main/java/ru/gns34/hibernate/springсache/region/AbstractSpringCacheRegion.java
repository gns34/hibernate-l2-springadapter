package ru.gns34.hibernate.springсache.region;

import java.util.Collections;
import java.util.Map;

import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.Region;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.gns34.hibernate.springсache.CacheAdapter;
import ru.gns34.hibernate.springсache.CacheConfig;

public abstract class AbstractSpringCacheRegion implements Region {

	protected final Logger log = LoggerFactory.getLogger(getClass());
	protected final CacheAdapter cache;
	private final String regionName;
    private final int lockTimeout;

    protected AbstractSpringCacheRegion(String regionName, CacheAdapter cache, CacheConfig config) {
        this.cache = cache;
        this.regionName = regionName;
        this.lockTimeout = config.getLockTimeouts(regionName);
    }

    @Override
    public String getName() {
        return regionName;
    }

    @Override
    public void destroy() throws CacheException {
    	cache.clear();
    }

    @Override
    public boolean contains(Object key) {
        return cache.contains(key);
    }
    
    @Override
    public long nextTimestamp() {
        return System.nanoTime();
    }

    @Override
    public int getTimeout() {
        return lockTimeout;
    }
    
	public CacheAdapter getCache() {
		return cache;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Map toMap() {
		return Collections.EMPTY_MAP; // Не поддерживается
	}

	@Override
    public long getSizeInMemory() {
        return -1; // Не поддерживается
    }

	@Override
    public long getElementCountInMemory() {
    	return -1; // Не поддерживается
    }

	@Override
    public long getElementCountOnDisk() {
    	return -1; // Не поддерживается
    }

}
