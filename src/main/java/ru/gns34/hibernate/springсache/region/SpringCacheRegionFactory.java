package ru.gns34.hibernate.springсache.region;

import java.util.Properties;

import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.NaturalIdRegion;
import org.hibernate.cache.spi.QueryResultsRegion;
import org.hibernate.cache.spi.RegionFactory;
import org.hibernate.cache.spi.TimestampsRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;

import com.mc.hibernate.memcached.region.MemcachedCollectionRegion;
import com.mc.hibernate.memcached.region.MemcachedEntityRegion;
import com.mc.hibernate.memcached.region.MemcachedNaturalIdRegion;
import com.mc.hibernate.memcached.region.MemcachedQueryResultsRegion;
import com.mc.hibernate.memcached.region.MemcachedTimestampsRegion;

import ru.gns34.hibernate.springсache.CacheAdapter;
import ru.gns34.hibernate.springсache.CacheAdapterImplSpring;
import ru.gns34.hibernate.springсache.CacheConfig;
import ru.gns34.hibernate.springсache.CacheNamespace;
import ru.gns34.hibernate.springсache.SpringContextAware;

/**
 * @author Гончаров Никита
 *
 */
public class SpringCacheRegionFactory implements RegionFactory {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCacheRegionFactory.class);

    private CacheManager cacheManager;
    private SessionFactoryOptions settings;

    public SpringCacheRegionFactory() {
    }
    
    public SpringCacheRegionFactory(Properties properties) {
    }

    @Override
    public void start(SessionFactoryOptions settings, Properties properties) throws CacheException {
        this.settings = settings;
        ApplicationContext context = SpringContextAware.getApplicationContext();
        if (context == null) {
        	throw new CacheException("Необходимо добавить бин ru.gns34.hibernate.springсache.SpringContextAware в контекст Spring");
        }
        try {
        	this.cacheManager = context.getBean(CacheManager.class);
        	Class<? extends CacheManager> cacheManagerClass = this.cacheManager.getClass();
			LOGGER.info("Используется {} как КЕШ менеджер из Spring context(Полное имя класса - {})", cacheManagerClass.getSimpleName(), cacheManagerClass);
        } catch (Exception e) {
            throw new CacheException("Невозможно получить CacheManager из контекста Spring", e);
        }
    }

    @Override
    public void stop() {
    	LOGGER.info("Поступил сигнал на остановку. Он будет проигнарирован, КЕШью управляет Spring");
    }

    @Override
    public boolean isMinimalPutsEnabledByDefault() {
        return true;
    }

    @Override
    public AccessType getDefaultAccessType() {
        return AccessType.READ_WRITE;
    }

    @Override
    public long nextTimestamp() {
        return System.nanoTime();
    }
    
    private CacheAdapter createCache(CacheNamespace cacheNamespace, String regionName, CacheConfig configurate) {
    	String cacheName = configurate.getCacheName(cacheNamespace, regionName);
		Cache cache = cacheManager.getCache(cacheName);
		LOGGER.debug("Для запроса {} получено имя КЕША '{}', реализация = {}", new Object[]{regionName, cacheName, cache});
		return new CacheAdapterImplSpring(cache);
	}
    
    private CacheConfig configurate(Properties properties) {
        return new CacheConfig(properties);
    }

    @Override
    public EntityRegion buildEntityRegion(String regionName, Properties properties, CacheDataDescription metadata) throws CacheException {
    	final CacheConfig config = configurate(properties);
    	final CacheAdapter createCache = createCache(CacheNamespace.ENTITY, regionName, config);
		return new MemcachedEntityRegion(regionName, createCache, settings, metadata, config);
    }

    @Override
	public NaturalIdRegion buildNaturalIdRegion(String regionName, Properties properties, CacheDataDescription metadata) throws CacheException {
    	final CacheConfig config = configurate(properties);
    	final CacheAdapter createCache = createCache(CacheNamespace.NATURALID, regionName, config);
        return new MemcachedNaturalIdRegion(regionName, createCache, settings, metadata, config);
    }

    @Override
    public CollectionRegion buildCollectionRegion(String regionName, Properties properties, CacheDataDescription metadata) throws CacheException {
    	final CacheConfig config = configurate(properties);
    	final CacheAdapter createCache = createCache(CacheNamespace.COLLECTION, regionName, config);
        return new MemcachedCollectionRegion(regionName, createCache, settings, metadata, config);
    }

    @Override
    public QueryResultsRegion buildQueryResultsRegion(String regionName, Properties properties) throws CacheException {
    	final CacheConfig config = configurate(properties);
    	final CacheAdapter createCache = createCache(CacheNamespace.QUERYRESULTS, regionName, config);
        return new MemcachedQueryResultsRegion(regionName, createCache, config);
    }

    @Override
    public TimestampsRegion buildTimestampsRegion(String regionName, Properties properties) throws CacheException {
    	final CacheConfig config = configurate(properties);
    	final CacheAdapter createCache = createCache(CacheNamespace.TIMESTAMPS, regionName, config);
        return new MemcachedTimestampsRegion(regionName, createCache, config);
    }

}
