package ru.gns34.hibernate.springсache;

/**
 * @author Гончаров Никита
 *
 */
public enum CacheNamespace {

	ENTITY("entity"),
	NATURALID("naturalid"),
	COLLECTION("collection"),
	QUERYRESULTS("query"),
	TIMESTAMPS("timestamps");
	
	private final String defaultName;
	
	private CacheNamespace(String defaultName) {
		this.defaultName = defaultName;
	}

	public String getDefaultName() {
		return defaultName;
	}
}
