package ru.gns34.hibernate.springсache;

import java.util.Properties;

public class CacheConfig {

    private static final String DEFAULT_CACHENAME_PREFIX = "hibernate-";

	private static final String PROP_PREFIX = "hibernate.springсache.";

    private static final String CACHE_NAME_PREFIX = "name.for.";
    private static final String CACHE_TIMEOUT = "lock.timeout";

    private static final String DEFAULT_CACHE_TIMEOUT = "300";

    private final Properties props;
    
    private static String getPropPrefixRegion(String cacheRegion) {
        return PROP_PREFIX + cacheRegion + ".";
    }

    public CacheConfig(Properties properties) {
        this.props = properties;
    }
    
	public String getCacheName(CacheNamespace entity, String regionName) {
		final String cacheNamePrefix = entity.getDefaultName();
		final String defaultName = props.getProperty(PROP_PREFIX + CACHE_NAME_PREFIX + cacheNamePrefix, cacheNamePrefix);
		return DEFAULT_CACHENAME_PREFIX 
				+ props.getProperty(getPropPrefixRegion(CACHE_NAME_PREFIX + regionName) + cacheNamePrefix, defaultName);
	}

    public int getLockTimeouts(String cacheRegion) {
        final String globalTimeout = props.getProperty(PROP_PREFIX + CACHE_TIMEOUT, DEFAULT_CACHE_TIMEOUT);
        try {
        	return Integer.parseInt(props.getProperty(getPropPrefixRegion(cacheRegion) + CACHE_TIMEOUT, globalTimeout));
		} catch (Exception e) {
			return Integer.parseInt(DEFAULT_CACHE_TIMEOUT);
		}
    }

}
