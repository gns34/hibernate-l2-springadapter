package ru.gns34.hibernate.springсache;

import org.hibernate.cache.CacheException;
import org.springframework.cache.Cache;

/**
 * @author Гончаров Никита
 *
 */
public interface CacheAdapter extends Cache {

	boolean contains(Object key) throws CacheException;
	
	Object getObject(Object key) throws CacheException;

	void lock(Object key) throws CacheException;

	void unlock(Object key)  throws CacheException;

	/* (non-Javadoc)
	 * @see org.springframework.cache.Cache#getName()
	 */
	@Override
	String getName() throws CacheException;

	/* (non-Javadoc)
	 * @see org.springframework.cache.Cache#getNativeCache()
	 */
	@Override
	Object getNativeCache() throws CacheException;

	/* (non-Javadoc)
	 * @see org.springframework.cache.Cache#get(java.lang.Object)
	 */
	@Override
	ValueWrapper get(Object key) throws CacheException;

	/* (non-Javadoc)
	 * @see org.springframework.cache.Cache#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	void put(Object key, Object value) throws CacheException;

	/* (non-Javadoc)
	 * @see org.springframework.cache.Cache#evict(java.lang.Object)
	 */
	@Override
	void evict(Object key) throws CacheException;

	/* (non-Javadoc)
	 * @see org.springframework.cache.Cache#clear()
	 */
	@Override
	void clear() throws CacheException;
	
}
