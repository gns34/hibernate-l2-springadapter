package ru.gns34.hibernate.springсache;

import org.hibernate.cache.CacheException;
import org.springframework.cache.Cache;

/**
 * @author Гончаров Никита
 *
 */
public class CacheAdapterImplSpring implements CacheAdapter {
	
	private final Cache cache;
	
	public CacheAdapterImplSpring(Cache cache) {
		this.cache = cache;
	}
	
	@Override
	public boolean contains(Object key) throws CacheException {
		return cache.get(key) != null;
	}
	
	@Override
	public Object getObject(Object key) throws CacheException {
		ValueWrapper valueWrapper = get(key);
		if (valueWrapper != null) {
			return valueWrapper.get();
		}
		return null;
	}

	@Override
	public void lock(Object key) throws CacheException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unlock(Object key) throws CacheException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		cache.clear();
	}

	@Override
	public String getName() {
		return cache.getName();
	}

	@Override
	public Object getNativeCache() {
		return cache.getNativeCache();
	}

	@Override
	public ValueWrapper get(Object key) {
		return cache.get(key);
	}

	@Override
	public void put(Object key, Object value) {
		cache.put(key, value);
	}

	@Override
	public void evict(Object key) {
		cache.evict(key);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cache == null) ? 0 : cache.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CacheAdapterImplSpring other = (CacheAdapterImplSpring) obj;
		if (cache == null) {
			if (other.cache != null) {
				return false;
			}
		} else if (!cache.equals(other.cache)) {
			return false;
		}
		return true;
	}

}
