/* Copyright 2015, the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mc.hibernate.memcached.region;

import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.TimestampsRegion;
import org.hibernate.engine.spi.SessionImplementor;

import ru.gns34.hibernate.springсache.CacheAdapter;
import ru.gns34.hibernate.springсache.CacheConfig;
import ru.gns34.hibernate.springсache.region.AbstractSpringCacheRegion;

public class MemcachedTimestampsRegion extends AbstractSpringCacheRegion implements TimestampsRegion {

    public MemcachedTimestampsRegion(String regionName, CacheAdapter cache, CacheConfig config) {
        super(regionName, cache, config);
    }

    @Override
    public Object get(SessionImplementor session, Object key) throws CacheException {
        return cache.getObject(key);
    }

    @Override
    public void put(SessionImplementor session, Object key, Object value) throws CacheException {
        cache.put(key, value);
    }

    @Override
    public void evict(Object key) throws CacheException {
        cache.evict(key);
    }

    @Override
    public void evictAll() throws CacheException {
        cache.clear();
    }

}
